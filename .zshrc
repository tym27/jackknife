export LESS="-r -iMSx4 -FX"
export PAGER=less
export TIME_STYLE=long-iso
export ZSH="${HOME}/.oh-my-zsh"
export PATH=${HOME}/.local/bin:/usr/local/bin:$PATH
export BAT_THEME="OneHalfLight"
ZSH_THEME="frisk"
plugins=(dotenv fasd git httpie pipenv postgres pyenv python sudo thefuck tmux vi-mode zsh-autosuggestions zsh-syntax-highlighting)
ENABLE_CORRECTION="true"

if [ -f $ZSH/oh-my-zsh.sh ]; then
  source $ZSH/oh-my-zsh.sh
fi

function l() {
    ls -Ahl --time-style=long-iso --color=always "$1" | less
}
eval $(thefuck --alias)

date -u "+%FT%TZ"
uname -n
uptime
