FROM debian:stable-slim

ENV TERM xterm
RUN apt-get update && apt-get install -y \
    curl \
    fasd \
    git \
    postgresql-client \
    procps \
    python3 \
    python3-dev \
    python3-pip \
    python3-psycopg \
    pipx \
    tmux \
    vim \
    wget \
    zip \
    zsh

RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# downlaad and install bat
ARG BATV=0.18.3
RUN wget -O /tmp/bat_${BATV}_amd64.deb https://github.com/sharkdp/bat/releases/download/v${BATV}/bat_${BATV}_amd64.deb
RUN dpkg -i /tmp/bat_${BATV}_amd64.deb

# set utf-8 locale
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

# download and install snowsql
ARG SNOWSQLV=1.2.20
ARG BOOTSTRAP=1.2
ENV SNOWSQL_DEST=/usr/local/bin
ENV SNOWSQL_LOGIN_SHELL=/dev/null


RUN wget -O /tmp/snowsql-${SNOWSQLV}-linux_x86_64.bash \
https://sfc-repo.snowflakecomputing.com/snowsql/bootstrap/${BOOTSTRAP}/linux_x86_64/snowsql-${SNOWSQLV}-linux_x86_64.bash 
RUN bash /tmp/snowsql-${SNOWSQLV}-linux_x86_64.bash
# update snowsql
RUN /usr/local/bin/snowsql -v

# install oh-my-zsh and related tools
RUN sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
RUN git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
RUN git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

# dba queries
RUN git clone https://gitlab.com/tym27/helpful-dba-queries.git /var/lib/postgresql/q

# upgrade pip and install ohter useful tools
RUN pipx install pip-tools 
RUN pipx install httpie
RUN pipx install thefuck

COPY . /root/
COPY hosts /etc/hosts
COPY snowsql-config /root/.snowsql/config
COPY pgst /usr/local/bin/pgst

VOLUME /projects
WORKDIR /projects

CMD zsh
