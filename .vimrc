syntax enable
filetype plugin indent on
set background=light
colorscheme one
set autoindent
set expandtab
set ignorecase
set linebreak
set nolist
set number
set shiftwidth=4
set smartcase
set softtabstop=4
set textwidth=0
set wrap
set wrapmargin=0
